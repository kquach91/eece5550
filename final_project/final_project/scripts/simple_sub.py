#!/usr/bin/env python

import rospy
from visualization_msgs.msg import Marker


def callback(msg):
    rospy.loginfo("test")


def pa2_sub():
    rospy.init_node('simple_sub', anonymous=True)

    rospy.Subscriber("visualization_marker", Marker, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    pa2_sub()