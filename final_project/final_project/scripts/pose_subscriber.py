#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry


def callback(msg):
    print "x: ", msg.pose.pose.position.x
    print "y: ", msg.pose.pose.position.y
    print "a: ", msg.pose.pose.orientation.w
    print "x_quat: ", msg.pose.pose.orientation.x
    print "y_quat: ", msg.pose.pose.orientation.y
    # print "z: ", msg.pose.pose.position.z
    rospy.set_param('initial_pose_x', msg.pose.pose.position.x)
    rospy.set_param('initial_pose_y', msg.pose.pose.position.y)
    rospy.set_param('initial_pose_a', msg.pose.pose.orientation.w)
    # rospy.set_param('initial_pose_z', msg.pose.pose.position.z)


def pose_sub():
    rospy.init_node('pose_subscriber', anonymous=True)

    rospy.Subscriber("/odom", Odometry, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    pose_sub()